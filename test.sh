#!/bin/bash

## this is not a runnable script, just a bunch of chunks not to forget tips


NUM_THREADS=3

BASE="$HOME"/tmp/circ_rrna_canonical

cd $BASE

## circ_explorer stuff

tophat2 -a 6 --microexon-search -m 2 -p $NUM_THREADS -G knownGene.gtf -o tophat \
    ucsc.hg19.canonical DOUBLE_10894_GGAGCC.fastq

bamToFastq -i tophat/unmapped.bam -fq tophat/unmapped.fastq

tophat2 -o tophat_fusion -p $NUM_THREADS --fusion-search --keep-fasta-order --bowtie1 \
    --no-coverage-search ucsc.hg19.canonical tophat/unmapped.fastq

# python CIRCexplorer.py -f tophat_fusion/accepted_hits.bam -g ucsc.hg19.canonical.fa -r knownGene.txt

echo 'Please copypaste the interval script and not import the interval from pypy'

## https://raw.githubusercontent.com/kepbod/interval/master/interval.py
python CIRCexplorer_with_interval.py -f tophat_fusion/accepted_hits.bam -g ucsc.hg19.canonical.fa -r knownGene.txt

## ciri stuff


## old-fashioned stuff

universes=(hsa_hg19_Jeck2013.bed_putative_circs_40.fa hsa_hg19_Memczak2013.bed_putative_circs_40.fa hsa_hg19_Salzman2013.bed_putative_circs_40.fa hsa_hg19_Zhang2013_H9.bed_putative_circs_40.fa)



universes=(hsa_hg19_Jeck2013.bed_putative_circs_40 hsa_hg19_Memczak2013.bed_putative_circs_40 hsa_hg19_Salzman2013.bed_putative_circs_40 hsa_hg19_Zhang2013_H9.bed_putative_circs_40)



for universe in ${universes[@]}
do
    bowtie-build "$universe" "$universe"
done

for f in *ebwt; do mv $f $(echo $f | sed 's/.fa//'); done

mkdir -p bowtie_old_fashioned

for universe in ${universes[@]}
do
    echo $(date) Bowtie on $sample to $universe start

    /soft/bin/bowtie \
        -q "$universe" \
        -S -p $NUM_THREADS -v 0 \
        --best -l 28 -k 1 \
        --un bowtie_old_fashioned/unaligned_"$universe".fasQ \
        --max bowtie_old_fashioned/ambiguous_"$universe".fasQ \
        --chunkmbs 128 \
        tophat/unmapped.fastq \
        bowtie_old_fashioned/mapping_circulars_"$universe".log \
        &> bowtie_old_fashioned/mapping_circulars_"$universe"_bowtie_summary.log

    grep circ bowtie_old_fashioned/mapping_circulars_"$universe".log  | grep -v "@" | cut -f 3 | sort \
        | uniq -c | less | sort -r > bowtie_old_fashioned/bowtie_based_counts_"$sample"_"$universe".txt
    grep circ bowtie_old_fashioned/mapping_circulars_"$universe".log  | grep -v "@" | cut -f 3,10  \
        | sort > bowtie_old_fashioned/bowtie_based_sequences_"$sample"_"$universe".txt

    echo $(date) Bowtie on $sample to $universe end

done


## ciri based

bwa index -a bwtsw ucsc.hg19.canonical.fa

bwa mem -k 15  -T 15 ucsc.hg19.canonical.fa DOUBLE_10894_GGAGCC.fastq > DOUBLE_10894_GGAGCC-se.sam

perl CIRI_eterno.pl  -I  DOUBLE_10894_GGAGCC-se.sam -O  DOUBLE_10894_GGAGCC-se.ciri \
    -F ucsc.hg19.canonical.fa --anno knownGene.gtf -S

perl CIRI_eterno.pl  -I  DOUBLE_10894_GGAGCC-se.sam -O  DOUBLE_10894_GGAGCC-se_no_strigency.ciri \
    -F ucsc.hg19.canonical.fa --anno knownGene.gtf --no-strigency -S
