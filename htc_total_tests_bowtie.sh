#!/bin/bash
# Izaskun Mallona

NUM_THREADS=3

WDIR="$HOME"/hct_total_bowtie
OUT="$WDIR"/out
# CODE=/imppc/labs/maplab/imallona/src/circ/third-party/memczak-based/circ_code
# CODE=/imppc/labs/maplab/imallona/src/circ/memczak-based/third-party/circ_code/unmapped2anchors.py
CODE='/imppc/labs/maplab/imallona/src/circ/memczak-based/third-party/circ_code'
# to be able to use the -M flag
BOWTIE=/soft/bio/bowtie2-2.0.0-beta5/bowtie2
BOWTIE_1=/soft/bin/bowtie



HCT_5H_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252528/SRR787296/SRR787296.sra
HCT_6H_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252529/SRR787297/SRR787297.sra
HCT_7H_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252530/SRR787298/SRR787298.sra

HCT_5H=SRR787296
HCT_6H=SRR787297
HCT_7H=SRR787298


# bam to fasta script that takes into account reverse strands
BAM_TO_FASTA=/imppc/labs/maplab/imallona/src/circ/bam_to_fasta.py


# param $1 the sra filename including the extension
# param $2 the folder
# param $3 the url to download 
function get_circulars_bowtie {

    folder=$2

    mkdir -p $folder

    cd $folder

    # wget $3

    bname=$(basename "$1")

    fname="${bname%.*}"
    
    # fastq-dump --split-3 $bname

    # find "$HOME"/circ_db/memczak* | xargs ln -s -t .
    # find "$HOME"/circ_db/jeck* | xargs ln -s -t .
    # find "$HOME"/circ_db/salzman* | xargs ln -s -t .
    # find "$HOME"/circ_db/zhang* | xargs ln -s -t .

    # ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.1.bt2  
    # ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.3.bt2  
    # ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.1.bt2
    # ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.2.bt2  
    # ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.4.bt2
    # ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.2.bt2
    # ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.fa
    
    # ln -s /home/labs/maplab/imallona/circ_db_36/circ36.* -t .

    # $BOWTIE -p"$NUM_THREADS" --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
    #     -q -U $fname.fastq 2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human

    # samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam
   
    # python $BAM_TO_FASTA unmapped_total.bam
    bedtools bamtofastq -i unmapped_total.bam -fq unmapped_total.fastq
    # mv unmapped_total.fa total_anchors.fa
 
    # blastn -query total_anchors.fa -db "jeck memczak salzman zhang" \
    #     -out blasted.tsv -outfmt 6 \
    #     -task blastn-short \
    #     -word_size 16 -perc_identity 0.95 \
    #     -num_threads "$NUM_THREADS" \
    #     -num_alignments 1

    # cut -f2 blasted.tsv | sort | uniq -c | sort -r > blasted_counts.txt

    #-f         that means the input is fasta
    #-S         SAM mode
    # -p 6      numcores 
    # -v 2      mismatches
    # -k 1      report only one valid alignment per read
    # chumkmbs  128 as used by Sergi (default is 64)
    # mind that the last item receives the stdout and the previous one is the input file
    /soft/bin/bowtie \
        -q circ36 \
        -S -p $NUM_THREADS -v 2 \
        --phred64-quals --best -l 28 -k 1 \
        --un unaligned.fasQ \
        --max ambiguous.fasQ \
        --chunkmbs 128 \
        unmapped_total.fastq \
        mapping_circulars.log

    grep circ mapping_circulars.log  | grep -v "@" | cut -f 3 | sort | uniq -c | less | sort -r > bowtie_based_counts.txt

}


echo 'Creating dir estructure'

mkdir -p $OUT

cd $OUT

echo 'Running'

get_circulars_bowtie "$HCT_5H".sra "$OUT"/"$HCT_5H" "$HCT_5H_URL"

# get_circulars "$HCT_6H".sra "$OUT"/"$HCT_6H" "$HCT_6H_URL"

# get_circulars "$HCT_7H".sra "$OUT"/"$HCT_7H" "$HCT_7H_URL"

