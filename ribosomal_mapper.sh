#!/bin/bash

function usage() {
    echo 
    echo Estimates the population of rRNA-species from an hiseq run (fastq)
    echo
    echo USAGE:
    echo
    echo
    echo PARAMETERS:
    echo

    exit 1
}

# if [ $# = 0 ]
# then
#     usage
# fi

# NUM_THREADS=$1
# BASE=$2
# MM=$3

NUM_THREADS=4
BASE="$HOME"/tmp/circ_rrna
MM=2
IDX_FA="$BASE"/rrna

SRC="/imppc/labs/maplab/imallona/src"

mkdir -p "$BASE"

cd "$BASE"

echo 'Retrieving the rRNA coordinates'

mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e \
    'SELECT genoName, genoStart,genoEnd, strand, repName,repClass,repFamily \
     FROM hg19.rmsk \
     WHERE repClass = "rRNA"' | awk '{if (NR!=1) {print}}' > "$BASE"/rrna.bed

wc -l "$BASE"/rrna.bed


echo "Fetching the sequences"

fastaFromBed -fi /biodata/indices/species/Hsapiens/ucsc.hg19.fa  \
    -bed "$BASE"/rrna.bed \
    -fo "$BASE"/rrna.fasta \
    -s 


bowtie-build "$BASE"/rrna.fasta \
       "$BASE"/rrna \
       > "$BASE"/rrna.bowtie_build_log


echo "Calling bowtie"

#-f         that means the input is fasta
#-S         SAM mode
# -p 6      numcores 
# -v 2      mismatches
# -k 1      report only one valid alignment per read
# chumkmbs  128 as used by Sergi (default is 64)
# mind that the last item receives the stdout and the previous one is the input file
/soft/bin/bowtie \
    -q \
    $IDX_FA \
    -S -p $NUM_THREADS -v $MM \
    --best -l 28 -k 1 \
    --un "$BASE"/unaligned.fasQ \
    --max "$BASE"/ambiguous.fasQ \
    --chunkmbs 128 \
    "$BASE"/1h_10893_AGGCCA.fastq \
    "$BASE"/1h_10893_AGGCCA.log.sam &> "$BASE"/1h_10893_AGGCCA.log

/soft/bin/bowtie \
    -q \
    $IDX_FA \
    -S -p $NUM_THREADS -v $MM \
    --best -l 28 -k 1 \
    --un "$BASE"/unaligned.fasQ \
    --max "$BASE"/ambiguous.fasQ \
    --chunkmbs 128 \
    "$BASE"/DOUBLE_10894_GGAGCC.fastq \
    "$BASE"/DOUBLE_10894_GGAGCC.log.sam &> "$BASE"/DOUBLE_10894_GGAGCC.log
