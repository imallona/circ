#!/bin bash
# 
# tophat
# circulars with fusion-anchor-test fine tuning
# 10 nov 2014



DATA_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252528/SRR787296/SRR787296.sra

WDIR=~/Desktop/circulars_tophat_fusion_anchor_test

bn=SRR787296

MEM_LIMIT=12000000
NUM_THREADS=6
FUSION_ANCHOR_LENGTH=10

BOWTIE_IDX=/biodata/indices/species/Hsapiens/ucsc.hg19

ulimit -Sv "$MEM_LIMIT"

mkdir -p $WDIR

cd $WDIR

wget $DATA_URL

fastq-dump --split-3 $bn.sra

echo 'db.host=genome-mysql.cse.ucsc.edu
db.user=genomep
db.password=password' > ~/.hg.conf
 
chmod 0600 ~/.hg.conf

genePredToGtf hg19 knownGene knownGene.gtf

tophat2 -a 6 --microexon-search -m 2 -p $NUM_THREADS -G knownGene.gtf     \
    -o  tophat $BOWTIE_IDX $bn.fastq

bamToFastq -i tophat/unmapped.bam -fq tophat/unmapped.fastq

tophat2 -o tophat_fusion -p $NUM_THREADS \
    --fusion-search --keep-fasta-order --bowtie1 \
    --fusion-anchor-length $FUSION_ANCHOR_LENGTH \
    --no-coverage-search $BOWTIE_IDX tophat/unmapped.fastq 


exit
# tests start

tophat2 -o tophat_fusion -p 6 \
    --fusion-anchor-length 10 \
    --mate-inner-dist 18 \
    --segment-length 15 \
    --fusion-search \
    --keep-fasta-order \
    --bowtie1 \
    --no-coverage-search \
    $BOWTIE_IDX \
    tophat/unmapped.fastq 


# testing here

tophat2 -o tophat_fusion_default_mate_inner_distance -p 6 \
    --fusion-anchor-length 10 \
    --segment-length 15 \
    --fusion-search \
    --keep-fasta-order \
    --bowtie1 \
    --no-coverage-search \
    $BOWTIE_IDX \
    tophat/unmapped.fastq 


tophat2 -o tophat_fusion_segment_length_10 -p 6 \
    --fusion-anchor-length 10 \
    --segment-length 10 \
    --fusion-search \
    --keep-fasta-order \
    --bowtie1 \
    --no-coverage-search \
    $BOWTIE_IDX \
    tophat/unmapped.fastq 
