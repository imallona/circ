#!/bin/bash

# Data getting and preprocessing for the circular RNAs project
# Positive control
#
# 10th September 2014
# Izaskun Mallona

WDIR="$HOME"/circ_positive
TOTAL=$WDIR/SRR650317
OUT="$WDIR"/out
CODE=/imppc/labs/maplab/imallona/src/circ/third-party/circ_code
# to be able to use the -M flag
BOWTIE=/soft/bio/bowtie2-2.0.0-beta5/bowtie2


# this performs the data download
# bash $SRC/circ/preprocess.sh

echo 'Creating dir estructure'

mkdir -p $OUT

echo 'Linking the bowtie2 indexes'
cd $OUT

ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.1.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.3.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.1.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.2.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.4.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.2.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.fa


# echo ">>> building bowtie2 index..."
# bowtie2-build CDR1as_locus.fa bt2_cdr1as_locus &> bt2_build.log

# echo ">>> aligning example reads"
# bowtie2 -p16 --very-sensitive --mm -M20 --score-min=C,-15,0 -x bt2_cdr1as_locus -f -U reads.fa 2> bt2_firstpass.log | samtools view -hbuS - | samtools sort - test_vs_cdr1as

echo 'getting the data'
cd $WDIR

# wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX218%2FSRX218203/SRR650317/SRR650317.sra
ln -s ~/circ_positive_no_splitted/SRR650317.sra

fastq-dump --split-3 SRR650317.sra
 
mv $TOTAL* $OUT

cd $OUT

echo ">>> aligning rna-total reads"
# $BOWTIE -p6 --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
#     -q -U SRR650317.fastq  2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human

# $BOWTIE -p6 --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
#     -q -1 SRR650317_1.fastq -2 SRR650317_2.fastq  2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human

$BOWTIE -p6 --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
    -q -U SRR650317_1.fastq,SRR650317_2.fastq  2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human


# echo ">>> get the unmapped"
# samtools view -hf 4 test_vs_cdr1as.bam | samtools view -Sb - > unmapped_test.bam


echo ">>> get the unmapped"
samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam




# echo ">>> split into anchors"
# ../unmapped2anchors.py unmapped_test.bam > test_anchors.qfa

echo ">>> split into anchors"
python "$CODE"/unmapped2anchors.py unmapped_total.bam > total_anchors.qfa


# echo ">>> run find_circ.py"
# mkdir "$OUT"/total_out
# $BOWTIE --reorder --mm -M20 --score-min=C,-15,0 -q -x bt2_cdr1as_locus -U test_anchors.qfa 2> bt2_secondpass.log | ../find_circ.py -r ../samples_example.txt -G . -p cdr1as_test_ -s test_out/sites.log > test_out/sites.bed 2> test_out/sites.reads


echo ">>> run find_circ.py"
mkdir "$OUT"/total_out
$BOWTIE --reorder --mm -M20 --score-min=C,-15,0 -q -x ucsc.hg19 -p6 \
    -U total_anchors.qfa 2> total_bt2_secondpass.log | "$CODE"/find_circ.py \
    -G /biodata/genomes/Hsapiens/ucsc.hg19.chr/ \
    -p total_hg19_ -s "$OUT"/sites.log > "$OUT"/sites.bed 2> "$OUT"/sites.reads



# echo ">>> compare to reference result. You should see 'overlap 1' here."
# "$CODE"/cmp_bed.py "$OUT"/total_out/sites.bed result_total.bed



# To get a reasonable set of circRNA candidates try:

grep circ "$OUT"/sites.bed | grep -v chrM | "$CODE"/sum.py -2,3 | "$CODE"/scorethresh.py -16 1 | "$CODE"/scorethresh.py -15 2 | "$CODE"/scorethresh.py -14 2 | "$CODE"/scorethresh.py 7 2 | "$CODE"/scorethresh.py 8,9 35 | "$CODE"/scorethresh.py -17 100000 > "$OUT"/circ_candidates.bed

