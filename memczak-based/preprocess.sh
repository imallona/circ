#!/bin/bash

# Data getting and preprocessin for the cirucular RNAs project

# 10th September 2014
# Izaskun Mallona

WDIR="$HOME"/circ
#hct116
POLYA=$WDIR/hct116_polya/SRR787303
TOTAL=$WDIR/hct116_total/SRR787296
#dko
DKO_POLYA=$WDIR/dko_polya/SRR787305
DKO_TOTAL=$WDIR/dko_polya/SRR787304

echo 'Set up'

mkdir -p $WDIR $WDIR/hct116_total $WDIR/hct116_polya $WDIR/dko_total $WDIR/dko_polya

echo 'Data retrieval'

wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252535/SRR787303/SRR787303.sra
mv SRR787303.sra $WDIR/hct116_polya

wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252528/SRR787296/SRR787296.sra
mv SRR787296.sra $WDIR/hct116_total

wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252536/SRR787304/SRR787304.sra
mv SRR787304.sra $WDIR/dko_total

wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252537/SRR787305/SRR787305.sra $WDIR/dko_polya
mv SRR787305.sra $WDIR/dko_polya

echo 'Generating fastqs'

cd $WDIR/hct116_polya/
fastq-dump "$POLYA".sra

cd $WDIR/hct116_total/
fastq-dump "$TOTAL".sra

cd $WDIR/dko_polya/
fastq-dump "$DKO_POLYA".sra

cd $WDIR/dko_total/
fastq-dump "$DKO_TOTAL".sra


echo 'Quality trimming'

sickle se -f "$POLYA".fastq  -t illumina -o "$POLYA"_sickled.fastq
sickle se -f "$TOTAL".fastq  -t illumina -o "$TOTAL"_sickled.fastq

sickle se -f "$DKO_POLYA".fastq  -t illumina -o "$DKO_POLYA"_sickled.fastq
sickle se -f "$DKO_TOTAL".fastq  -t illumina -o "$DKO_TOTAL"_sickled.fastq

echo 'Quality check and QC plots generation'

fastqc "$POLYA"_sickled.fastq  -o $WDIR/hct116_polya
fastqc "$TOTAL"_sickled.fastq  -o $WDIR/hct116_total

fastqc "$DKO_POLYA"_sickled.fastq  -o $WDIR/dko_polya
fastqc "$DKO_TOTAL"_sickled.fastq  -o $WDIR/dko_total


