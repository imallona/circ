#!/bin/bash

# Data getting and preprocessin for the cirucular RNAs project

# 10th September 2014
# Izaskun Mallona

WDIR="$HOME"/circ_dko
OUT="$WDIR"/out_dko
CODE=/imppc/labs/maplab/imallona/src/circ/third-party/circ_code
#dko
DKO_POLYA=$WDIR/dko_polya/SRR787305
DKO_TOTAL=$WDIR/dko_total/SRR787304


# this performs the data download
# bash $SRC/circ/preprocess.sh

echo 'Creating dir estructure'

mkdir -p $OUT

echo 'Linking the bowtie2 indexes'
cd $OUT

ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.1.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.3.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.1.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.2.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.4.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.2.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.fa


# echo ">>> building bowtie2 index..."
# bowtie2-build CDR1as_locus.fa bt2_cdr1as_locus &> bt2_build.log

# echo ">>> aligning example reads"
# bowtie2 -p16 --very-sensitive --mm -M20 --score-min=C,-15,0 -x bt2_cdr1as_locus -f -U reads.fa 2> bt2_firstpass.log | samtools view -hbuS - | samtools sort - test_vs_cdr1as

echo ">>> aligning rna-total reads"
bowtie2 -p6 --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
    --phred33 \
    -q -U "$DKO_TOTAL".fastq  2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human

# echo ">>> get the unmapped"
# samtools view -hf 4 test_vs_cdr1as.bam | samtools view -Sb - > unmapped_test.bam


echo ">>> get the unmapped"
samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam




# echo ">>> split into anchors"
# ../unmapped2anchors.py unmapped_test.bam > test_anchors.qfa

echo ">>> split into anchors"
python "$CODE"/unmapped2anchors.py unmapped_total.bam > total_anchors.qfa


# echo ">>> run find_circ.py"
# mkdir "$OUT"/total_out
# bowtie2 --reorder --mm -M20 --score-min=C,-15,0 -q -x bt2_cdr1as_locus -U test_anchors.qfa 2> bt2_secondpass.log | ../find_circ.py -r ../samples_example.txt -G . -p cdr1as_test_ -s test_out/sites.log > test_out/sites.bed 2> test_out/sites.reads


echo ">>> run find_circ.py"
mkdir "$OUT"/total_out
bowtie2 --reorder --mm -M20 --score-min=C,-15,0 -q -x ucsc.hg19 -p6 \
    --phred33 \
    -U total_anchors.qfa 2> "$OUT"/total_out/total_bt2_secondpass.log | "$CODE"/find_circ.py \
    -G /biodata/genomes/Hsapiens/ucsc.hg19.chr/ \
    -p total_hg19_ -s "$OUT"/total_out/sites.log > "$OUT"/total_out/sites.bed 2> "$OUT"/total_out/sites.reads



# echo ">>> compare to reference result. You should see 'overlap 1' here."
# "$CODE"/cmp_bed.py "$OUT"/total_out/sites.bed result_total.bed



# To get a reasonable set of circRNA candidates try:

grep circ "$OUT"/total_out/sites.bed | grep -v chrM | "$CODE"/sum.py -2,3 | "$CODE"/scorethresh.py -16 1 | "$CODE"/scorethresh.py -15 2 | "$CODE"/scorethresh.py -14 2 | "$CODE"/scorethresh.py 7 2 | "$CODE"/scorethresh.py 8,9 35 | "$CODE"/scorethresh.py -17 100000 > "$OUT"/total_out/circ_candidates.bed




#########


# for polya data #####################


echo ">>> aligning rna-polya reads"
bowtie2 -p6 --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
    --phred33 \
    -q -U "$DKO_POLYA".fastq  2> polya_bt2_firstpass.log | samtools view -hbuS - | samtools sort - polya_vs_human

# echo ">>> get the unmapped"
# samtools view -hf 4 test_vs_cdr1as.bam | samtools view -Sb - > unmapped_test.bam


echo ">>> get the unmapped"
samtools view -hf 4 polya_vs_human.bam | samtools view -Sb - > unmapped_polya.bam




# echo ">>> split into anchors"
# ../unmapped2anchors.py unmapped_test.bam > test_anchors.qfa

echo ">>> split into anchors"
python "$CODE"/unmapped2anchors.py unmapped_polya.bam > polya_anchors.qfa


## run till here


# echo ">>> run find_circ.py"
# mkdir "$OUT"/polya_out
# bowtie2 --reorder --mm -M20 --score-min=C,-15,0 -q -x bt2_cdr1as_locus -U test_anchors.qfa 2> bt2_secondpass.log | ../find_circ.py -r ../samples_example.txt -G . -p cdr1as_test_ -s test_out/sites.log > test_out/sites.bed 2> test_out/sites.reads


echo ">>> run find_circ.py"
mkdir "$OUT"/polya_out
bowtie2 --reorder --mm -M20 --score-min=C,-15,0 -q -x ucsc.hg19 -p6 \
    --phred33 \
    -U polya_anchors.qfa 2> "$OUT"/polya_out/bt2_secondpass.log | "$CODE"/find_circ.py \
    -G /biodata/genomes/Hsapiens/ucsc.hg19.chr/ \
    -p polya_hg19_ -s "$OUT"/polya_out/sites.log > "$OUT"/polya_out/sites.bed 2> "$OUT"/polya_out/sites.reads



# echo ">>> compare to reference result. You should see 'overlap 1' here."
# "$CODE"/cmp_bed.py "$OUT"/polya_out/sites.bed result_polya.bed


# To get a reasonable set of circRNA candidates try:

grep circ "$OUT"/polya_out/sites.bed | grep -v chrM | "$CODE"/sum.py -2,3 | "$CODE"/scorethresh.py -16 1 | "$CODE"/scorethresh.py -15 2 | "$CODE"/scorethresh.py -14 2 | "$CODE"/scorethresh.py 7 2 | "$CODE"/scorethresh.py 8,9 35 | "$CODE"/scorethresh.py -17 100000 > "$OUT"/polya_out/circ_candidates.bed



