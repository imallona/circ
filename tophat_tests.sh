#!/bin bash

bn=SRR787296

MEM_LIMIT=12000000
NUM_THREADS=6

BOWTIE_IDX=/biodata/indices/species/Hsapiens/ucsc.hg19

ulimit -Sv "$MEM_LIMIT"

echo 'db.host=genome-mysql.cse.ucsc.edu
db.user=genomep
db.password=password' > ~/.hg.conf
 
# the file's permissions must be user-only
chmod 0600 ~/.hg.conf

## hg18/UCSC Known Genes
genePredToGtf hg19 knownGene knownGene.gtf


tophat2 -a 6 --microexon-search -m 2 -p $NUM_THREADS -G knownGene.gtf     \
    -o  tophat $BOWTIE_IDX     $bn.fastq

bamToFastq -i tophat/unmapped.bam -fq tophat/unmapped.fastq

# tophat2 -o tophat_fusion -p $NUM_THREADS \
#     --segment-length 15 \
#     --fusion-search --keep-fasta-order --bowtie1 \
#     --no-coverage-search $BOWTIE_IDX tophat/unmapped.fastq 


# tophat2 -o tophat_fusion -p $NUM_THREADS \
#     --segment-length 12 \
#     --fusion-search --keep-fasta-order --bowtie1 \
#     --no-coverage-search $BOWTIE_IDX tophat/unmapped.fastq 


# /soft/bio/tophat-2.0.8/bin/tophat2 -o tophat_fusion_older -p $NUM_THREADS \
#     --segment-length 12 \
#     --fusion-search --keep-fasta-order --bowtie1 \
#     --no-coverage-search $BOWTIE_IDX tophat/unmapped.fastq 


/soft/bio/tophat-2.0.8/bin/tophat2  -o tophat_fusion_bowtie2 -p $NUM_THREADS \
    --segment-length 12 \
    --fusion-search --keep-fasta-order \
    --no-coverage-search $BOWTIE_IDX tophat/unmapped.fastq 

# these don't work with the hct116 total data...
