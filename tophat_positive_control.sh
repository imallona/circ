#!/bin bash
#
# postive control for the circexplorer circ determination
# 22 oct 2014


DATA_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX682%2FSRX682269/SRR1552724/SRR1552724.sra

WDIR=~/Desktop/circulars_tophat_test

bn=SRR1552724

MEM_LIMIT=12000000
NUM_THREADS=6

BOWTIE_IDX=/biodata/indices/species/Hsapiens/ucsc.hg19

ulimit -Sv "$MEM_LIMIT"


cd $WDIR

wget $DATA_URL

fastq-dump --split-3 $bn.sra

echo 'db.host=genome-mysql.cse.ucsc.edu
db.user=genomep
db.password=password' > ~/.hg.conf
 
chmod 0600 ~/.hg.conf

genePredToGtf hg19 knownGene knownGene.gtf

tophat2 -a 6 --microexon-search -m 2 -p $NUM_THREADS -G knownGene.gtf     \
    -o  tophat $BOWTIE_IDX     $bn.fastq

bamToFastq -i tophat/unmapped.bam -fq tophat/unmapped.fastq

tophat2 -o tophat_fusion -p $NUM_THREADS \
    --fusion-search --keep-fasta-order --bowtie1 \
    --no-coverage-search $BOWTIE_IDX tophat/unmapped.fastq 
