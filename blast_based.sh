#!/bin/bash
# Izaskun Mallona

NUM_THREADS=3

WDIR="$HOME"/circ_db
OUT="$WDIR"/out
# CODE=/imppc/labs/maplab/imallona/src/circ/third-party/memczak-based/circ_code
# CODE=/imppc/labs/maplab/imallona/src/circ/memczak-based/third-party/circ_code/unmapped2anchors.py
CODE='/imppc/labs/maplab/imallona/src/circ/memczak-based/third-party/circ_code'
# to be able to use the -M flag
BOWTIE=/soft/bio/bowtie2-2.0.0-beta5/bowtie2


POLYA=$WDIR/SRR787303
TOTAL=$WDIR/hct116_total/SRR787296
#dko
DKO_POLYA=$WDIR/SRR787305
DKO_TOTAL=$WDIR/SRR787304

# bam to fasta script that takes into account reverse strands
BAM_TO_FASTA=/imppc/labs/maplab/imallona/src/circ/bam_to_fasta.py

# POLYA=SRR787303
# TOTAL=SRR787296

echo 'Creating dir estructure'

mkdir -p $OUT

echo 'Linking the bowtie2 indexes'
cd $OUT

ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.1.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.3.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.1.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.2.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.4.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.2.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.fa

# echo 'getting the data'
# cd $WDIR

# wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252535/SRR787303/SRR787303.sra

# wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252528/SRR787296/SRR787296.sra


# fastq-dump --split-3 $POLYA.sra
# fastq-dump --split-3 $TOTAL.sra






formatdb -t jeck -i hsa_hg19_Jeck2013.bed_putative_circs.fa \
    -p F -o T -n jeck

formatdb -t memczak -i hsa_hg19_Memczak2013.bed_putative_circs.fa \
    -p F -o T -n memczak

formatdb -t salzman -i hsa_hg19_Salzman2013.bed_putative_circs.fa \
    -p F -o T -n salzman

formatdb -t zhang -i hsa_hg19_Zhang2013_H9.bed_putative_circs.fa\
    -p F -o T -n zhang

ln -s ../circ_positive/out/total_anchors.qfa positive_control_total_anchors.qfa

awk '{if(NR%4==1) {printf(">%s\n",substr($0,2));} else if(NR%4==2) print;}' positive_control_total_anchors.qfa > positive_control_total_anchors.fa

# blastn -query positive_control_total_anchors.fa -db memczak -out test

# blastn -query positive_control_total_anchors.fa -db "memczak_cd19 memczak_hek293 memczak_cd34 memczak_neutr" \
#     -out positive_against_memczak -outfmt 6 \
#     -task blastn-short \
#     # -word_size 18 \ 
#     -num_threads 4

blastn -query positive_control_total_anchors.fa -db "jeck memczak salzman zhang" \
    -out positive_against_memczak_restrictive -outfmt 6 \
    -task blastn-short \
    -word_size 16 -perc_identity 0.95 \
    -num_threads 6 \
    -num_alignments 1


cut -f2 positive_against_memczak_restrictive | sort | uniq -c | sort -r > positive_against_memczak_restrictive_counts.txt



## hct runs












# exit

### now data from hct116 and dko


function get_circulars {
    bname=$(basename "$1")

    fname="${bname%.*}"
    fastq-dump --split-3 $bname

    find ../memczak* | xargs ln -s -t .
    find ../memczak* | xargs ln -s -t .
    find ../salzman* | xargs ln -s -t .
    find ../zhang* | xargs ln -s -t .

    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.1.bt2  
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.3.bt2  
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.1.bt2
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.2.bt2  
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.4.bt2
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.2.bt2
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.fa


    $BOWTIE -p"$NUM_THREADS" --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
        -q -U $fname.fastq 2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human

    samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam
    python "$CODE"/unmapped2anchors.py unmapped_total.bam > total_anchors.qfa
    
    awk '{if(NR%4==1) {printf(">%s\n",substr($0,2));} else if(NR%4==2) print;}' total_anchors.qfa > total_anchors.fa
    blastn -query total_anchors.fa -db "jeck memczak salzman zhang" \
        -out blasted.tsv -outfmt 6 \
        -task blastn-short \
        -word_size 16 -perc_identity 0.95 \
        -num_threads "$NUM_THREADS" \
        -num_alignments 1

    cut -f2 blasted.tsv | sort | uniq -c | sort -r > blasted_counts.txt
}


function get_circulars_from_fastq_no_anchors {
    bname=$(basename "$1")

    fname="${bname%.*}"
    # fastq-dump --split-3 $bname

    find ../memczak* | xargs ln -s -t .
    find ../jeck* | xargs ln -s -t .
    find ../salzman* | xargs ln -s -t .
    find ../zhang* | xargs ln -s -t .

    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.1.bt2  
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.3.bt2  
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.1.bt2
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.2.bt2  
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.4.bt2
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.2.bt2
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.fa


    $BOWTIE -p"$NUM_THREADS" --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
        -q -U $fname.fastq 2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human

    samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam
    # python "$CODE"/unmapped2anchors.py unmapped_total.bam > total_anchors.qfa
    python $BAM_TO_FASTA unmapped_total.bam
    mv unmapped_total.fa total_anchors.fa

    # awk '{if(NR%4==1) {printf(">%s\n",substr($0,2));} else if(NR%4==2) print;}' total_anchors.qfa > total_anchors.fa
    blastn -query total_anchors.fa -db "jeck memczak salzman zhang" \
        -out blasted.tsv -outfmt 6 \
        -task blastn-short \
        -word_size 16 -perc_identity 0.95 \
        -num_threads "$NUM_THREADS" \
        -num_alignments 1

    cut -f2 blasted.tsv | sort | uniq -c | sort -r > blasted_counts.txt
}



wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252535/SRR787303/SRR787303.sra
# mv SRR787303.sra $WDIR/hct116_polya

wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252528/SRR787296/SRR787296.sra
# mv SRR787296.sra $WDIR/hct116_total

wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252536/SRR787304/SRR787304.sra
# mv SRR787304.sra $WDIR/dko_total

wget ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252537/SRR787305/SRR787305.sra 
# mv SRR787305.sra $WDIR/dko_polya

# echo 'Generating fastqs'

# # cd $WDIR/hct116_polya/
# fastq-dump --split-3 "$POLYA".sra

# # cd $WDIR/hct116_total/
# fastq-dump --split-3 "$TOTAL".sra

# # cd $WDIR/dko_polya/
# fastq-dump --split-3 "$DKO_POLYA".sra

# # cd $WDIR/dko_total/
# fastq-dump --split-3 "$DKO_TOTAL".sra


mkdir -p $WDIR/hct116_polya/

mkdir -p $WDIR/hct116_total/

mkdir -p $WDIR/dko_polya/

mkdir -p $WDIR/dko_total/

cd $WDIR/hct116_polya/
get_circulars "$POLYA".sra

cd $WDIR/hct116_total/
get_circulars "$TOTAL".sra

cd $WDIR/dko_polya/
get_circulars "$DKO_POLYA".sra

cd $WDIR/dko_total/
get_circulars "$DKO_TOTAL".sra





# # echo ">>> aligning rna-total reads"

# mkdir -p $WDIR/hct116_polya/

# cd  $WDIR/hct116_polya/

# ln -s ../SRR787303.fastq

# $BOWTIE -p6 --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
#     -q -U *fastq 2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human
# samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam
# python "$CODE"/unmapped2anchors.py unmapped_total.bam > total_anchors.qfa

# blastn -query total_anchors.fa -db "memczak_cd19 memczak_hek293 memczak_cd34 memczak_neutr" \
#     -out blasted.tsv -outfmt 6 \
#     -task blastn-short \
#     -word_size 16 -perc_identity 0.95 \
#     -num_threads 6 \
#     -num_alignments 1


# cd $WDIR/hct116_total/
# $BOWTIE -p6 --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
#     -q -U *fastq 2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human
# samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam
# python "$CODE"/unmapped2anchors.py unmapped_total.bam > total_anchors.qfa

# cd $WDIR/dko_polya/
# $BOWTIE -p6 --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
#     -q -U *fastq 2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human
# samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam
# python "$CODE"/unmapped2anchors.py unmapped_total.bam > total_anchors.qfa

# cd $WDIR/dko_total/
# $BOWTIE -p6 --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
#     -q -U *fastq 2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human
# samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam
# python "$CODE"/unmapped2anchors.py unmapped_total.bam > total_anchors.qfa

# ## 
 
# # mv $POLYA.fastq

# # cd $OUT

# # echo ">>> aligning rna-total reads"

# # $BOWTIE -p6 --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
# #     -q -U SRR446726_1.fastq,SRR446726_2.fastq,SRR172836_1.fastq,SRR172836_2.fastq 2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human


# # # echo ">>> get the unmapped"
# # # samtools view -hf 4 test_vs_cdr1as.bam | samtools view -Sb - > unmapped_test.bam


# # echo ">>> get the unmapped"
# # samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam




# # # echo ">>> split into anchors"
# # # ../unmapped2anchors.py unmapped_test.bam > test_anchors.qfa

# # echo ">>> split into anchors"
# # python "$CODE"/unmapped2anchors.py unmapped_total.bam > total_anchors.qfa


# # # echo ">>> run find_circ.py"
# # # mkdir "$OUT"/total_out
# # # $BOWTIE --reorder --mm -M20 --score-min=C,-15,0 -q -x bt2_cdr1as_locus -U test_anchors.qfa 2> bt2_secondpass.log | ../find_circ.py -r ../samples_example.txt -G . -p cdr1as_test_ -s test_out/sites.log > test_out/sites.bed 2> test_out/sites.reads


# # echo ">>> run find_circ.py"
# # $BOWTIE --reorder --mm -M20 --score-min=C,-15,0 -q -x ucsc.hg19 -p6 \
# #     -U total_anchors.qfa 2> total_bt2_secondpass.log | "$CODE"/find_circ.py \
# #     -G /biodata/genomes/Hsapiens/ucsc.hg19.chr/ \
# #     -p total_hg19_ -s "$OUT"/sites.log > "$OUT"/sites.bed 2> "$OUT"/sites.reads


# # # To get a reasonable set of circRNA candidates try:

# # grep circ "$OUT"/sites.bed | grep -v chrM | "$CODE"/sum.py -2,3 | "$CODE"/scorethresh.py -16 1 | "$CODE"/scorethresh.py -15 2 | "$CODE"/scorethresh.py -14 2 | "$CODE"/scorethresh.py 7 2 | "$CODE"/scorethresh.py 8,9 35 | "$CODE"/scorethresh.py -17 100000 > "$OUT"/circ_candidates.bed

