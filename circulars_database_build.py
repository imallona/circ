#!/usr/bin/env python

import urllib
import os
import pybedtools
import time

BEDFILES = ['http://circbase.mdc-berlin.de/download/hsa_hg19_Jeck2013.bed',
            'http://circbase.mdc-berlin.de/download/hsa_hg19_Memczak2013.bed',
            'http://circbase.mdc-berlin.de/download/hsa_hg19_Salzman2013.bed',
            'http://circbase.mdc-berlin.de/download/hsa_hg19_Zhang2013_H9.bed']

DATA_PATH = '/imppc/labs/maplab/imallona/data/circ'
BASE_PATH = '/home/labs/maplab/imallona/circ_db'
HG19 = '/biodata/indices/species/Hsapiens/ucsc.hg19.fa'
SLICE_SIZE = 40

def process_bed(bed):
    foo =  pybedtools.BedTool(os.path.join(BASE_PATH, bed.split('/')[-1])) 
    print(bed + ' was opened')
    foo_copy = []
    for line in str(foo).split('\n'):
        if not '#' in line:
            foo_copy.append(line)
    
    # foo_copy = str(foo).split('\n')
    # time.sleep(1)
    seq = foo.sequence(fi = HG19, s = True)
    seq_content = open(seq.seqfn, 'r').readlines()
    result = ''
    for i in range(len(seq_content)):
        if ((i % 2 == 0 ) and (i < len(seq_content) - 1)): 
            circ_id = seq_content[i].strip() + '_' +  str(foo_copy[i/2]).split('\t')[3]
            # circ_id = seq_content[i].strip() + '_' + ''.join(foo_copy[i/2].split('\t')[0:2]) + '_'  +  str(foo_copy[i/2]).split('\t')[3]
            content = seq_content[i +1 ].strip()
            circ_seq =  content[-SLICE_SIZE:] + content[0:SLICE_SIZE]
            result += '%s\n%s\n' %(circ_id, circ_seq)
    with open(os.path.join(BASE_PATH, bed.split('/')[-1] + '_putative_circs_%s.fa'%SLICE_SIZE), 'w') as fh:
        fh.write(result)

if not os.path.exists(BASE_PATH):
    os.makedirs(BASE_PATH)

for bed in BEDFILES:
    if not os.path.exists(os.path.join(BASE_PATH, bed.split('/')[-1])):
        try:
            urllib.urlretrieve(bed, filename = os.path.join(BASE_PATH, bed.split('/')[-1]))
        except:
            print('No longer available at circbase server, using local copy')
            os.symlink(os.path.join(DATA_PATH, bed.split('/')[-1]), os.path.join(BASE_PATH, bed.split('/')[-1]))

for bed in BEDFILES:
    print('Processing ' + bed)
    process_bed(bed)
        


# raise

# result = ''
# for i in range(len(seq_content)):
#     if ((i % 2 == 0 ) and (i < len(seq_content) - 1)): 
#         circ_id = seq_content[i].strip() + '_' + str(foo[i/2]).split('\t')[3]
#         content = seq_content[i +1 ].strip()
#         circ_seq = content[0:SLICE_SIZE] + content[-SLICE_SIZE:]
#         result += '%s\n%s\n' %(circ_id, circ_seq)

# foo =  pybedtools.BedTool(os.path.join(BASE_PATH, bed.split('/')[-1]))    
# seq = foo.sequence(fi = HG19, s = True)
# seq_content = open(seq.seqfn).readlines()

# result = ''
# for i in range(len(seq_content)):
#     if ((i % 2 == 0 ) and (i < len(seq_content) - 1)): 
#         circ_id = seq_content[i].strip() + '_' + str(foo[i/2]).split('\t')[3]
#         content = seq_content[i +1 ].strip()
#         circ_seq =  content[-SLICE_SIZE:] + content[0:SLICE_SIZE]
#         result += '%s\n%s\n' %(circ_id, circ_seq)

# with open(os.path.join(BASE_PATH, bed.split('/')[-1] + '_putative_circs.fa'), 'w') as fh:
#     fh.write(result)
        
