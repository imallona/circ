#!/bin/bash
#
# Izaskun Mallona
#
# 17 nov 2014
# relies on bowtie_based_recomputation.sh as some of the data fetching was done there

NUM_THREADS=3
BOWTIE_IDX=/biodata/indices/species/Hsapiens/ucsc.hg19

BOWTIE_WDIR="$HOME"/yaizas_circs_again
BOWTIE_OUT="$BOWTIE_WDIR"/out
# CODE=/imppc/labs/maplab/imallona/src/circ/third-party/memczak-based/circ_code
# CODE=/imppc/labs/maplab/imallona/src/circ/memczak-based/third-party/circ_code/unmapped2anchors.py
CODE='/imppc/labs/maplab/imallona/src/circ/memczak-based/third-party/circ_code'
# to be able to use the -M flag
BOWTIE=/soft/bio/bowtie2-2.0.0-beta5/bowtie2
BOWTIE_1=/soft/bin/bowtie

WDIR="$HOME"/yaiza_tophat
OUT="$WDIR"/out

HCT_5H_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252528/SRR787296/SRR787296.sra
HCT_6H_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252529/SRR787297/SRR787297.sra
HCT_7H_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252530/SRR787298/SRR787298.sra

HCT_5H=SRR787296
HCT_6H=SRR787297
HCT_7H=SRR787298

# further data

SRR787303_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252535/SRR787303/SRR787303.sra
SRR787303=SRR787303
SRR787304_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252536/SRR787304/SRR787304.sra
SRR787304=SRR787304
SRR787305_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252537/SRR787305/SRR787305.sra
SRR787305=SRR787305

# for f in *ebwt; do mv $f $(echo $f | sed 's/.fa//'); done


# param $1 the sra filename including the extension
# param $2 the folder
# param $3 the url to download 
function get_circulars_bowtie {

    folder=$2

    mkdir -p $folder

    cd $folder

    wget $3

    bname=$(basename "$1")

    fname="${bname%.*}"
    
    fastq-dump --split-3 $bname

    # find "$HOME"/circ_db/memczak* | xargs ln -s -t .
    # find "$HOME"/circ_db/jeck* | xargs ln -s -t .
    # find "$HOME"/circ_db/salzman* | xargs ln -s -t .
    # find "$HOME"/circ_db/zhang* | xargs ln -s -t .

    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.1.bt2  
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.3.bt2  
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.1.bt2
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.2.bt2  
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.4.bt2
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.2.bt2
    ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.fa
    
    # ln -s /home/labs/maplab/imallona/circ_db_36/circ36.* -t .

    ln -s /imppc/labs/maplab/imallona/map/circ_db_36/* -t .

    $BOWTIE -p"$NUM_THREADS" --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
        -q -U $fname.fastq 2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human

    samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam
   
    bedtools bamtofastq -i unmapped_total.bam -fq unmapped_total.fastq
    # mv unmapped_total.fa total_anchors.fa
 
    # blastn -query total_anchors.fa -db "jeck memczak salzman zhang" \
    #     -out blasted.tsv -outfmt 6 \
    #     -task blastn-short \
    #     -word_size 16 -perc_identity 0.95 \
    #     -num_threads "$NUM_THREADS" \
    #     -num_alignments 1

    # cut -f2 blasted.tsv | sort | uniq -c | sort -r > blasted_counts.txt

    #-f         that means the input is fasta
    #-S         SAM mode
    # -p 6      numcores 
    # -v 2      mismatches
    # -k 1      report only one valid alignment per read
    # chumkmbs  128 as used by Sergi (default is 64)
    # mind that the last item receives the stdout and the previous one is the input file
    /soft/bin/bowtie \
        -q circ36 \
        -S -p $NUM_THREADS -v 2 \
        --phred64-quals --best -l 28 -k 1 \
        --un unaligned.fasQ \
        --max ambiguous.fasQ \
        --chunkmbs 128 \
        unmapped_total.fastq \
        mapping_circulars.log

    grep circ mapping_circulars.log  | grep -v "@" | cut -f 3 | sort | uniq -c | less | sort -r > bowtie_based_counts.txt
    grep circ mapping_circulars.log  | grep -v "@" | cut -f 3,10  | sort > bowtie_based_sequences.txt

}


# echo 'Creating dir estructure'

# mkdir -p $BOWTIE_OUT

# cd $BOWTIE_OUT

# # echo 'Building the 36 bp-long universes'

# # python $IHOME/src/circ/circulars_database_build.py

# echo 'Running'

# get_circulars_bowtie "$HCT_5H".sra "$BOWTIE_OUT"/"$HCT_5H" "$HCT_5H_URL"

# get_circulars_bowtie "$HCT_6H".sra "$BOWTIE_OUT"/"$HCT_6H" "$HCT_6H_URL"

# get_circulars_bowtie "$HCT_7H".sra "$BOWTIE_OUT"/"$HCT_7H" "$HCT_7H_URL"

# get_circulars_bowtie "$SRR787303".sra "$BOWTIE_OUT"/"$SRR787303" "$SRR787303_URL"

# get_circulars_bowtie "$SRR787304".sra "$BOWTIE_OUT"/"$SRR787304" "$SRR787304_URL"

# get_circulars_bowtie "$SRR787305".sra "$BOWTIE_OUT"/"$SRR787305" "$SRR787305_URL"

# generating the universe for 30bp-long elements

# python "$IHOME"/src/circ/circulars_database_build.py

# building the bowtie indices separately

cd $WDIR

cp -r $HOME/circ_db/*30*.fa .

universes=(hsa_hg19_Jeck2013.bed_putative_circs_30.fa hsa_hg19_Memczak2013.bed_putative_circs_30.fa hsa_hg19_Salzman2013.bed_putative_circs_30.fa hsa_hg19_Zhang2013_H9.bed_putative_circs_30.fa)

for universe in ${universes[@]}
do
    bowtie-build "$universe" "$universe"
done

for f in *ebwt; do mv $f $(echo $f | sed 's/.fa//'); done

echo Tophating

echo 'db.host=genome-mysql.cse.ucsc.edu
db.user=genomep
db.password=password' > .hg.conf
 
chmod 0600 .hg.conf

genePredToGtf hg19 knownGene knownGene.gtf

# samples=(SRR787296 SRR787297 SRR787298 SRR787303 SRR787304 SRR787305)
samples=(SRR787296 SRR787297 SRR787303 SRR787304 SRR787305)

for sample in ${samples[@]}
do
    echo $(date) Tophat on $sample start
    
    mkdir -p "$OUT"/"$sample"
    cd "$OUT"/"$sample"

    tophat2 -a 6 --microexon-search -m 2 -p $NUM_THREADS -G ../../knownGene.gtf     \
        -o  tophat $BOWTIE_IDX     "$BOWTIE_OUT"/"$sample"/"$sample".fastq

    bamToFastq -i tophat/unmapped.bam -fq tophat/unmapped.fastq
    
    echo $(date) Tophat on $sample end

    mkdir -p bowtie
    
    for universe in ${universes[@]}
    do
        echo $(date) Bowtie on $sample to $universe start

        /soft/bin/bowtie \
            -q ../../"$universe" \
            -S -p $NUM_THREADS -v 0 \
            --phred64-quals --best -l 28 -k 1 \
            --un bowtie/unaligned_"$universe".fasQ \
            --max bowtie/ambiguous_"$universe".fasQ \
            --chunkmbs 128 \
            tophat/unmapped.fastq \
            bowtie/mapping_circulars_"$universe".log

        grep circ bowtie/mapping_circulars_"$universe".log  | grep -v "@" | cut -f 3 | sort \
            | uniq -c | less | sort -r > bowtie/bowtie_based_counts_"$sample"_"$universe".txt
        grep circ bowtie/mapping_circulars_"$universe".log  | grep -v "@" | cut -f 3,10  \
            | sort > bowtie/bowtie_based_sequences_"$sample"_"$universe".txt

        echo $(date) Bowtie on $sample to $universe end

    done

done

