

NUM_THREADS=4

WDIR="$HOME"/hct_total_bowtie
OUT="$WDIR"/out
# CODE=/imppc/labs/maplab/imallona/src/circ/third-party/memczak-based/circ_code
# CODE=/imppc/labs/maplab/imallona/src/circ/memczak-based/third-party/circ_code/unmapped2anchors.py
CODE='/imppc/labs/maplab/imallona/src/circ/memczak-based/third-party/circ_code'
# to be able to use the -M flag
BOWTIE=/soft/bio/bowtie2-2.0.0-beta5/bowtie2
BOWTIE_1=/soft/bin/bowtie



HCT_5H_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252528/SRR787296/SRR787296.sra
HCT_6H_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252529/SRR787297/SRR787297.sra
HCT_7H_URL=ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX%2FSRX252%2FSRX252530/SRR787298/SRR787298.sra

HCT_5H=SRR787296
HCT_6H=SRR787297
HCT_7H=SRR787298

bname=SRR787305

# bam to fasta script that takes into account reverse strands
BAM_TO_FASTA=/imppc/labs/maplab/imallona/src/circ/bam_to_fasta.py

cd /home/labs/maplab/imallona/hct_total_bowtie/out/SRR787305_bis

fastq-dump --split-3 $bname.sra

ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.1.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.3.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.1.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.2.bt2  
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.4.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.rev.2.bt2
ln -s /biodata/indices/species/Hsapiens/ucsc.hg19.fa

$BOWTIE -p"$NUM_THREADS" --very-sensitive --mm -M20 --score-min=C,-15,0 -x ucsc.hg19 \
    -q -U $fname.fastq 2> total_bt2_firstpass.log | samtools view -hbuS - | samtools sort - total_vs_human

samtools view -hf 4 total_vs_human.bam | samtools view -Sb - > unmapped_total.bam

bedtools bamtofastq -i unmapped_total.bam -fq unmapped_total.fastq

find /home/labs/maplab/imallona/hct_total_bowtie/out/SRR787296/circ* | xargs ln -s -t .


/soft/bin/bowtie \
        -q circ36 \
        -S -p $NUM_THREADS -v 2 \
        --phred64-quals --best -l 28 -k 1 \
        --un unaligned.fasQ \
        --max ambiguous.fasQ \
        --chunkmbs 128 \
        unmapped_total.fastq \
        mapping_circulars.log

grep circ mapping_circulars.log  | grep -v "@" | cut -f 3 | sort | uniq -c | less | sort -r > bowtie_based_counts.txt
